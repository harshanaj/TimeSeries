#pragma once

#include "TimeSeries.h"

namespace ts
{
    class CorrelationCal
    {
    public:
        CorrelationCal(TimeSeries &timeSeries1, TimeSeries &timeSeries2);
        CorrelationCal(const CorrelationCal & correlationCal);
        CorrelationCal &operator=(const CorrelationCal & correlationCal);
        virtual ~CorrelationCal();

        double getCorrelation();

    private:
        TimeSeries &m_timeSeries1;
        TimeSeries &m_timeSeries2;
    };
}
