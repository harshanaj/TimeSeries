
#include <iostream>

#include "Filters.h"

ts::Filters::Filters(TimeSeries &timeSeries, int period)
        : m_timeSeries(timeSeries),
          m_numPeriods(period)
{
}

ts::Filters::Filters(const ts::Filters &filters)
        : m_timeSeries(filters.m_timeSeries),
          m_numPeriods(filters.m_numPeriods)
{
}

ts::Filters &ts::Filters::operator = (const ts::Filters &filters)
{
    if (this != &filters)
    {
        m_timeSeries = filters.m_timeSeries;
        m_numPeriods = filters.m_numPeriods;
    }
    return *this;
}

ts::Filters::~Filters()
{
}

std::vector<double> ts::Filters::calMovingAvg()
{
    std::vector<double> movingAvg;
    double sum = 0;

    int size = m_timeSeries.getSize();
    for (int i = 0; i < size; ++i)
    {
        sum += m_timeSeries.getValue(i);
        if (i >= m_numPeriods)
        {
            movingAvg.push_back(sum / m_numPeriods);
            sum -= m_timeSeries.getValue(i-m_numPeriods);
        }
    }
    return movingAvg;
}