#include <iostream>

#include "TimeSeries.h"
#include "CorrelationCal.h"
#include "Filters.h"

int main()
{
    ts::TimeSeries timeSeries1;
    timeSeries1.add(1, 100);
    timeSeries1.add(2, 200);
    timeSeries1.add(3, 300);
    timeSeries1.add(4, 400);
    timeSeries1.add(5, 500);
    timeSeries1.add(6, 600);
    timeSeries1.add(7, 700);

    ts::TimeSeries timeSeries2;
    timeSeries2.add(1, 1000);
    timeSeries2.add(2, 1100);
    timeSeries2.add(3, 1200);
    timeSeries2.add(4, 1300);
    timeSeries2.add(5, 1400);
    timeSeries2.add(6, 1500);
    timeSeries2.add(7, 1600);

    ts::CorrelationCal correlationCal(timeSeries1, timeSeries2);
    ts::Filters filters(timeSeries1, 5);

    std::cout << "value @ : " << timeSeries1.getValue(3) << std::endl;
    std::cout << "average : " << timeSeries1.getAverage() << std::endl;
    std::cout << "variance : " << timeSeries1.getVariance() << std::endl;
    std::cout << "correlation : " << correlationCal.getCorrelation() << std::endl;

    return 0;
}